import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Socks implements Runnable {
    public static final byte SOCKS_VERSION = 0x05;
    public static final byte NO_AUTH_REQ = 0x00;
    public static final byte CMD_CONNECT = 0x01;

    public static final byte ATYP_IPV4 = 0x01;
    public static final byte ATYP_IPV6 = 0x04;
    public static final byte ATYP_DOMAIN = 0x03;

    public static final byte REP_OK = 0x00;

    public static final int DEFAULT_BUFFER_SIZE = 8192;
    public static final int DEFAULT_HEADER_SIZE = 10;
    public static final int DEFAULT_PORT = 1080;
    public static final String DEFAULT_IP = "127.0.0.1";

    int port = DEFAULT_PORT;
    String host = DEFAULT_IP;

    private Selector selector;

    static class Attachment {
        Boolean auth = false;
        Boolean connect = false;

        ByteBuffer in;
        ByteBuffer out;

        SelectionKey peer;

        Attachment () {}
        Attachment (SelectionKey key) { peer = key; }
    }

    @Override
    public void run () {
        try {
            selector = Selector.open();
            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.socket().bind(new InetSocketAddress(host, port));
            serverChannel.register(selector, serverChannel.validOps());

            while (selector.select() > -1) {
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (key.isValid()) {
                        try {
                            if (key.isAcceptable()) {
                                accept(key);
                            }
                            else if (key.isConnectable()) {
                                connect(key);
                            }
                            else if (key.isReadable()) {
                                read(key);
                            }
                            else if (key.isWritable()) {
                                write(key);
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            close(key);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
        finally {
            if (selector.isOpen()) {
                Iterator<SelectionKey> iterator = selector.keys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    if (key.isValid() && key.channel().isOpen()) {
                        try {
                            key.channel().close();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    key.cancel();
                }

                try {
                    selector.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void accept (SelectionKey key)
            throws IOException, ClosedChannelException
    {
        SocketChannel newChannel = ((ServerSocketChannel) key.channel()).accept();
        newChannel.configureBlocking(false);
        newChannel.register(key.selector(), SelectionKey.OP_READ);
    }

    private void read (SelectionKey key)
            throws IOException, UnknownHostException, ClosedChannelException
    {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        if (attachment == null) {
            key.attach(attachment = new Attachment());
            attachment.in = ByteBuffer.allocate(DEFAULT_HEADER_SIZE);
        }

        if (channel.read(attachment.in) < 1) {
            close(key);
        }
        else if (attachment.peer == null) {
            readHeader(key, attachment);
        }
        else {
            attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_WRITE);
            key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
            attachment.in.flip();
        }
    }

    private void readHeader (SelectionKey key, Attachment attachment)
            throws IllegalStateException, IOException, UnknownHostException, ClosedChannelException
    {
        ByteBuffer header = attachment.in;

        byte[] tempo = new byte[header.position()];
        header.rewind();
        header.get(tempo);

        if (!attachment.auth) {
            if (header.get(0) == SOCKS_VERSION) {
                byte auth_num = header.get(1);

                for (int i = 2; i < (2 + auth_num); ++i)
                {
                    if (header.get(i) == NO_AUTH_REQ) {
                        attachment.out = ByteBuffer.allocate(2);
                        attachment.out.clear();
                        byte[] reply = new byte[] { SOCKS_VERSION, NO_AUTH_REQ };
                        attachment.out.put(reply).flip();

                        attachment.auth = true;
                        attachment.in.clear();
                        key.interestOps(SelectionKey.OP_WRITE);

                        return;
                    }
                }
            }

            System.err.println("Header(size:" + tempo.length +"): ");
            for (byte b : tempo) {
                System.err.printf("0x%02X ", b);
            }
            System.err.println();

            throw new IllegalStateException("Bad Request");
        }
        else if (!attachment.connect) {
            if (header.get(1) == CMD_CONNECT) {
                switch (header.get(3)) {
                    case (ATYP_IPV4): {
                        SocketChannel peer = SocketChannel.open();
                        peer.configureBlocking(false);

                        byte[] addr = new byte[] { header.get(4), header.get(5), header.get(6), header.get(7) };
                        int port = (((0xFF & header.get(8)) << 8) + (0xFF & header.get(9)));

                        peer.connect(new InetSocketAddress(InetAddress.getByAddress(addr), port));
                        SelectionKey peerKey = peer.register(key.selector(), SelectionKey.OP_CONNECT);
                        key.interestOps(0);
                        attachment.peer = peerKey;
                        Attachment peerAttachment = new Attachment(key);
                        peerKey.attach(peerAttachment);

                        attachment.in = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
                        return;
                    }
                    case (ATYP_DOMAIN): {
                        System.err.println("DOMAIN");
                        throw new IllegalStateException("Bad Request");
                    }
                    case (ATYP_IPV6):
                    default: {
                        System.err.println("IPV6");
                        throw new IllegalStateException("Bad Request");
                    }
                }
            }

            System.err.printf("Requested method: 0x%02X", header.get(1));
            System.err.println();
            throw new IllegalStateException("Bad Request");
        }
    }

    private void write (SelectionKey key)
            throws IOException
    {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
       if (channel.write(attachment.out) == -1) {
            close(key);
        }
        else if (attachment.out.remaining() ==  0) {
            if (attachment.peer == null) {
                if (!attachment.connect) {
                    attachment.out.clear();
                    key.interestOps(SelectionKey.OP_READ);
                }
                else {
                    close(key);
                }
            }
            else {
                attachment.out.clear();
                attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_READ);
                key.interestOps(key.interestOps() ^ SelectionKey.OP_WRITE);
            }
        }
    }

    private void connect (SelectionKey key)
            throws IOException
    {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        channel.finishConnect();

        attachment.in = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
        attachment.connect = true;
        int port = channel.socket().getPort();
        byte[] addr = channel.socket().getInetAddress().getAddress();
        byte[] okReply = new byte[] { SOCKS_VERSION, REP_OK, 0x00, ATYP_IPV4, addr[0], addr[1], addr[2], addr[3], (byte) (port & 0xFF), (byte) (port >> 8 & 0xFF) };
        attachment.in.put(okReply).flip();

        attachment.out = ((Attachment) attachment.peer.attachment()).in;
        ((Attachment) attachment.peer.attachment()).out = attachment.in;

        attachment.peer.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
        key.interestOps(0);
    }

    private void close (SelectionKey key)
            throws IOException
    {
        key.cancel();
        key.channel().close();
        SelectionKey peerKey = ((Attachment) key.attachment()).peer;
        if (peerKey != null) {
            ((Attachment)peerKey.attachment()).peer = null;

            if((peerKey.interestOps() & SelectionKey.OP_WRITE) == 0) {
                ((Attachment)peerKey.attachment()).out.flip();
            }
            peerKey.interestOps(SelectionKey.OP_WRITE);
        }
    }

    public static void main (String[] args)
    {
        Socks server = new Socks();

        if (args.length > 0) {
            server.port = Integer.parseInt(args[0]);
        }

        server.run();
    }
}
