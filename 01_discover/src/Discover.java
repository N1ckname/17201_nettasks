import java.io.IOException;
import java.net.*;
import java.util.*;

public class Discover {
    static final int BUF_LIMIT = 10;
    static final int CLIENT_TTL = 10000;
    static final int CHECK_DELAY = 5000;
    static final int TIMEOUT = 2000;

    static long lastTimeCheck = 0;
    static long lastSend = 0;
    static byte[] buf = new byte[BUF_LIMIT];

    public static void main(String[] args) {

        String ip;
        int port;
        if (args.length > 1) {
            ip = args[0].trim();
            port = Integer.parseInt(args[1].trim());
        } else {
            System.err.println("No ip address provided.");
            return;
        }

        try {
            MulticastSocket sock = new MulticastSocket(port);
            InetAddress group = InetAddress.getByName(ip);
            sock.setSoTimeout(TIMEOUT);

            try{
                sock.joinGroup(group);
            }
            catch (IOException e) {
                System.err.println("Can't connect to the group.");
                return;
            }

            Map<String, Long> clients = new HashMap<>();
            byte[] id = generateName();
            String idStr = new String(id);

            while (true) {
                try {
                    if (System.in.available() > 4) {
                        Scanner sc = new Scanner(System.in);
                        String input = sc.nextLine();
                        if (input != null) {
                            if ("quit".equals(input)) {
                                System.out.println("Exiting program.");
                                sock.close();

                                return;
                            }
                        }
                    }
                } catch (IOException ignored) {
                }

                DatagramPacket in = new DatagramPacket(buf, buf.length);
                try{
                    sock.receive(in);
                    String inMsg = new String(in.getData());
                    clients.put(inMsg, System.currentTimeMillis());
                }
                catch (SocketTimeoutException ignored) {
                }

                if ((System.currentTimeMillis() - lastTimeCheck)  > CHECK_DELAY ) {
                    System.out.println("This client name: " + idStr);
                    System.out.println("Clients found:");
                    Collection<Map.Entry<String, Long>> clientsIds = clients.entrySet();
                    clientsIds.removeIf(stringLongEntry -> (System.currentTimeMillis() - stringLongEntry.getValue()) > CLIENT_TTL);

                    if (clients.size() == 0) {
                        System.out.println("No clients found");
                    }
                    else {
                        for (Map.Entry<String, Long> stringLongEntry : clients.entrySet()) {
                            System.out.println(stringLongEntry.getKey());
                        }
                        System.out.println();
                    }

                    lastTimeCheck = System.currentTimeMillis();
                }

                if ((System.currentTimeMillis() - lastSend) > TIMEOUT) {
                    DatagramPacket msg = new DatagramPacket(id, id.length, group, port);
                    sock.send(msg);

                    lastSend = System.currentTimeMillis();
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static byte[] generateName() {
        byte[] bytes = new byte[10];
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        random.nextBytes(bytes);

        for (int i = 0; i < bytes.length; ++i) {
            bytes[i] = (byte) (33 + (Math.abs(bytes[i]) % 64));
        }

        return bytes;
    }
}
